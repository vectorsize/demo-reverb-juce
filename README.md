# Demo Reverb

This reverb is modeled after the Dattorro paper called [Reverberator and Other Filters](https://ccrma.stanford.edu/~dattorro/EffectDesignPart1.pdf).  
More specifically on a [Dattorro Pure Data implementation](http://tre.ucsd.edu/wordpress/wp-content/uploads/2016/05/pdreverb.zip) by Tom Erbe aka SoundHack.

There are no further modifications in this code.