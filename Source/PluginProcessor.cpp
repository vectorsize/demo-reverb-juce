/*
  ==============================================================================

    This file contains the basic framework code for a JUCE plugin processor.

  ==============================================================================
*/

#include "PluginEditor.h"
#include "PluginProcessor.h"

//==============================================================================
ReverbDemoAudioProcessor::ReverbDemoAudioProcessor()
#ifndef JucePlugin_PreferredChannelConfigurations
    : AudioProcessor(
          BusesProperties()
#if !JucePlugin_IsMidiEffect
#if !JucePlugin_IsSynth
              .withInput("Input", juce::AudioChannelSet::stereo(), true)
#endif
              .withOutput("Output", juce::AudioChannelSet::stereo(), true)
#endif
      )
#endif
{
  addParameter(kPredelay = new juce::AudioParameterFloat(
                   "Predelay", "Predelay", 0.0f, 1.0f, reverb.kPreDelay));
  addParameter(kBandwidth = new juce::AudioParameterFloat(
                   "Bandwidth", "Bandwidth", 0.0f, 1.0f, reverb.kBandwidth));
  addParameter(kDamping = new juce::AudioParameterFloat(
                   "Damping", "Damping", 0.0f, 1.0f, reverb.kDamping));
  addParameter(kTime = new juce::AudioParameterFloat("Time", "Time", 0.0f, 1.0f,
                                                     reverb.kTime));
  addParameter(kMix = new juce::AudioParameterFloat("Mix", "Mix", 0.0f, 1.0f,
                                                    reverb.kDrywet));
    updateParams();
}

ReverbDemoAudioProcessor::~ReverbDemoAudioProcessor() {}

//==============================================================================
const juce::String ReverbDemoAudioProcessor::getName() const {
  return JucePlugin_Name;
}

bool ReverbDemoAudioProcessor::acceptsMidi() const {
#if JucePlugin_WantsMidiInput
  return true;
#else
  return false;
#endif
}

bool ReverbDemoAudioProcessor::producesMidi() const {
#if JucePlugin_ProducesMidiOutput
  return true;
#else
  return false;
#endif
}

bool ReverbDemoAudioProcessor::isMidiEffect() const {
#if JucePlugin_IsMidiEffect
  return true;
#else
  return false;
#endif
}

double ReverbDemoAudioProcessor::getTailLengthSeconds() const { return 0.0; }

int ReverbDemoAudioProcessor::getNumPrograms() {
  return 1; // NB: some hosts don't cope very well if you tell them there are 0
            // programs, so this should be at least 1, even if you're not really
            // implementing programs.
}

int ReverbDemoAudioProcessor::getCurrentProgram() { return 0; }

void ReverbDemoAudioProcessor::setCurrentProgram(int index) {}

const juce::String ReverbDemoAudioProcessor::getProgramName(int index) {
  return {};
}

void ReverbDemoAudioProcessor::changeProgramName(int index,
                                                 const juce::String &newName) {}

//==============================================================================
void ReverbDemoAudioProcessor::prepareToPlay(double sampleRate,
                                             int samplesPerBlock) {
}

void ReverbDemoAudioProcessor::updateParams() {
    reverb.setPredelay(*kPredelay);
    reverb.setBandwidth(*kBandwidth);
    reverb.setDamping(*kDamping);
    reverb.setTime(*kTime);
    reverb.setMix(*kMix);
}

void ReverbDemoAudioProcessor::releaseResources() {
  // When playback stops, you can use this as an opportunity to free up any
  // spare memory, etc.
}

#ifndef JucePlugin_PreferredChannelConfigurations
bool ReverbDemoAudioProcessor::isBusesLayoutSupported(
    const BusesLayout &layouts) const {
#if JucePlugin_IsMidiEffect
  juce::ignoreUnused(layouts);
  return true;
#else
  // This is the place where you check if the layout is supported.
  // In this template code we only support mono or stereo.
  // Some plugin hosts, such as certain GarageBand versions, will only
  // load plugins that support stereo bus layouts.
  if (layouts.getMainOutputChannelSet() != juce::AudioChannelSet::mono() &&
      layouts.getMainOutputChannelSet() != juce::AudioChannelSet::stereo())
    return false;

    // This checks if the input layout matches the output layout
#if !JucePlugin_IsSynth
  if (layouts.getMainOutputChannelSet() != layouts.getMainInputChannelSet())
    return false;
#endif

  return true;
#endif
}
#endif

void ReverbDemoAudioProcessor::processBlock(juce::AudioBuffer<float> &buffer,
                                            juce::MidiBuffer &midiMessages) {
  juce::ScopedNoDenormals noDenormals;
  auto totalNumInputChannels = getTotalNumInputChannels();
  auto totalNumOutputChannels = getTotalNumOutputChannels();

updateParams();
    
  for (auto i = totalNumInputChannels; i < totalNumOutputChannels; ++i)
    buffer.clear(i, 0, buffer.getNumSamples());

  auto *inBufferL = buffer.getReadPointer(0);
  auto *inBufferR = buffer.getReadPointer(1);
  auto *outBufferL = buffer.getWritePointer(0);
  auto *outBufferR = buffer.getWritePointer(1);

  for (auto sample = 0; sample < buffer.getNumSamples(); ++sample) {
    reverb.processSamples(inBufferL[sample], inBufferR[sample]);
    outBufferL[sample] = reverb.outputL;
    outBufferR[sample] = reverb.outputR;
  }
}

//==============================================================================
bool ReverbDemoAudioProcessor::hasEditor() const {
  return false; // (change this to false if you choose to not supply an editor)
}

juce::AudioProcessorEditor *ReverbDemoAudioProcessor::createEditor() {
  return new ReverbDemoAudioProcessorEditor(*this);
}

//==============================================================================
void ReverbDemoAudioProcessor::getStateInformation(
    juce::MemoryBlock &destData) {
  // You should use this method to store your parameters in the memory block.
  // You could do that either as raw data, or use the XML or ValueTree classes
  // as intermediaries to make it easy to save and load complex data.
}

void ReverbDemoAudioProcessor::setStateInformation(const void *data,
                                                   int sizeInBytes) {
  // You should use this method to restore your parameters from this memory
  // block, whose contents will have been created by the getStateInformation()
  // call.
}

//==============================================================================
// This creates new instances of the plugin..
juce::AudioProcessor *JUCE_CALLTYPE createPluginFilter() {
  return new ReverbDemoAudioProcessor();
}
