//
//  Reverb.hpp
//
//  Created by Victor Saiz on 26/04/2021.
//

#ifndef Reverb_hpp
#define Reverb_hpp

#include "lib/allpass.h"
#include "lib/defines.h"
#include "lib/delay.h"
#include "lib/onepole.h"
#include <stdint.h>

namespace demo {

// Buffers / memory
// ---
typedef Allpass<float, ALLPASS_BUFFER_SIZE> AllpassBuff;
typedef Allpass<float, ALLPASS_BUFFER_SIZE> ModAllpassBuff;
typedef Allpass<float, ALLPASS_BUFFER_SIZE> Allpass2TapBuff;
typedef DelayLine<float, DELAY_BUFFER_SIZE> Delay;
typedef DelayLine<float, PREDELAY_BUFFER_SIZE> PreDelay;

// Diffusion L
static float allPass1LBuffer[ALLPASS_BUFFER_SIZE];
static float allPass2LBuffer[ALLPASS_BUFFER_SIZE];
static float allPass3LBuffer[ALLPASS_BUFFER_SIZE];
static float allPass4LBuffer[ALLPASS_BUFFER_SIZE];
// Diffusion R
static float allPass1RBuffer[ALLPASS_BUFFER_SIZE];
static float allPass2RBuffer[ALLPASS_BUFFER_SIZE];
static float allPass3RBuffer[ALLPASS_BUFFER_SIZE];
static float allPass4RBuffer[ALLPASS_BUFFER_SIZE];
// Modulated Allpass
static float modAllPassLBuffer[ALLPASS_BUFFER_SIZE];
static float modAllPassRBuffer[ALLPASS_BUFFER_SIZE];
// output Allpass
static float allPassLBuffer[ALLPASS_BUFFER_SIZE];
static float allPassRBuffer[ALLPASS_BUFFER_SIZE];

class Reverb {
private:
  // Predelays
  PreDelay mPredelayL;
  PreDelay mPredelayR;
  // Diffusion L
  // ---
  // Allpass(T* buffer, uint32_t delIndex, float gain)
  AllpassBuff mAllPass1L{allPass1LBuffer, AP1_TIME, 0.75f};
  AllpassBuff mAllPass2L{allPass2LBuffer, AP2_TIME, 0.75f};
  AllpassBuff mAllPass3L{allPass3LBuffer, AP3_TIME, 0.625f};
  AllpassBuff mAllPass4L{allPass4LBuffer, AP4_TIME, 0.625f};

  // Diffusion R
  // ---
  // Allpass(T* buffer, uint32_t delIndex, float gain)
  AllpassBuff mAllPass1R{allPass1RBuffer, AP1_TIME, 0.75f};
  AllpassBuff mAllPass2R{allPass2RBuffer, AP2_TIME, 0.75f};
  AllpassBuff mAllPass3R{allPass3RBuffer, AP3_TIME, 0.625f};
  AllpassBuff mAllPass4R{allPass4RBuffer, AP4_TIME, 0.625f};

  // In
  // ---
  // ModAllpass	(T* buffer, uint32_t delIndex, float gain, float lfoFreq)
  ModAllpassBuff mModAllpassL{modAllPassLBuffer, MODAPL_TIME, 0.7f, 0.1f};
  ModAllpassBuff mModAllpassR{modAllPassRBuffer, MODAPR_TIME, 0.7f, 0.7f};

  // Reverb Tank
  // ---
  Delay mDela;
  Delay mDelc;
  Delay mDeld;
  Delay mDelf;

  // Tank out
  // ---
  // Allpass2Tap(T* buffer, uint32_t delIndex, float gain)
  Allpass2TapBuff mAllpass2TapL{allPassLBuffer, APL_TAP1_TIME, 0.5f};
  Allpass2TapBuff mAllpass2TapR{allPassRBuffer, APR_TAP1_TIME, 0.5f};

  OnePole mFilterInL;
  OnePole mFilterInR;
  OnePole mFilterOutL;
  OnePole mFilterOutR;

  const float KGainAttenuator = 0.025f;

public:
  Reverb() = default;
  ~Reverb() = default;

  void setup();
  void processSamples(const float sL, const float sR);

  float kPreDelay = 0.01f;
  float kPreDelaySmooth = 0.f;
  void setPredelay(const float ctl);

  float kBandwidth = 0.6f;
  void setBandwidth(const float ctl);

  float kDamping = 0.8f;
  void setDamping(const float ctl);

  float kTime = 0.6f;
  void setTime(const float ctl);

  float kDrywet = 0.85f;
  void setMix(const float ctl);

  float outputL = 0.0f;
  float outputR = 0.0f;
  const float mDelayFloor = 0.001f;

  DISALLOW_COPY_AND_ASSIGN(Reverb);
};
} // namespace demo

#endif /* Reverb_hpp */
