//
//  onepole.h
//
//  Created by Victor Saiz on 08/11/2020.
// based on rpole~ - real one-pole filter from PureData source

#ifndef onepole_h
#define onepole_h

#include <math.h>

class OnePole {
public:
  OnePole() { mLastSample = 0.0f; };

  OnePole(const float Fc) {
    mLastSample = 0.0f;
    setFc(Fc);
  };

  inline void setFc(const float Fc) { mCoef = Fc; }

  inline float process(const float nextSample) {
    float outSample = mCoef * mLastSample + nextSample;
    mLastSample = outSample;
    return outSample;
  }

private:
  float mLastSample;
  float mCoef;
};

#endif /* onepole_h */
