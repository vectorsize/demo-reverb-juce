#pragma once
#ifndef Definitions
#define Definitions

#include <stdint.h>

#define DISALLOW_COPY_AND_ASSIGN(TypeName)                                     \
  TypeName(TypeName &) = delete;                                               \
  void operator=(TypeName) = delete;

#ifndef SAMPLE_RATE
#define SAMPLE_RATE 44100
#endif

#ifndef MS_TO_SAMPS
#define MS_TO_SAMPS(ms)                                                        \
  static_cast<uint32_t>(static_cast<float>(ms * 0.001f) *                      \
                        static_cast<float>(SAMPLE_RATE))
#endif

#ifndef MS_TO_SAMPSF
#define MS_TO_SAMPSF(ms)                                                       \
  static_cast<float>(static_cast<float>(ms * 0.001f) *                         \
                     static_cast<float>(SAMPLE_RATE))
#endif

namespace demo {

// helpers
template <typename T> T msToSamps(const float ms) {
  return ms * static_cast<T>(SAMPLE_RATE) / static_cast<T>(1000);
}

template <typename T>
inline T map(const T value, const T istart, const T istop, const T ostart,
             const T ostop) {
  return static_cast<T>(ostart) +
         (static_cast<T>(ostop) - static_cast<T>(ostart)) *
             ((static_cast<T>(value) - static_cast<T>(istart)) /
              (static_cast<T>(istop) - static_cast<T>(istart)));
}
template <typename T> inline T inv(float v) {
  return (static_cast<T>(v) - 1) * -1;
}

// MS to samples defines
constexpr const uint32_t PREDELAY_BUFFER_SIZE = MS_TO_SAMPS(2000.f);
constexpr const uint32_t DELAY_BUFFER_SIZE = MS_TO_SAMPS(200.f);
constexpr const uint32_t ALLPASS_BUFFER_SIZE = MS_TO_SAMPS(100.f);

// Diffusion
constexpr const uint32_t AP1_TIME = MS_TO_SAMPS(4.771f);
constexpr const uint32_t AP2_TIME = MS_TO_SAMPS(3.595f);
constexpr const uint32_t AP3_TIME = MS_TO_SAMPS(12.73f);
constexpr const uint32_t AP4_TIME = MS_TO_SAMPS(9.307f);

// Tank
constexpr const uint32_t MODAPL_TIME = MS_TO_SAMPS(30.51f);
constexpr const uint32_t MODAPR_TIME = MS_TO_SAMPS(22.58f);

constexpr const float DELF_TAP1 = MS_TO_SAMPSF(125.f);
constexpr const float DELC_TAP1 = MS_TO_SAMPSF(106.28f);

constexpr const float DELA_TAP1 = MS_TO_SAMPSF(141.69f);
constexpr const float DELD_TAP1 = MS_TO_SAMPSF(149.62f);

constexpr const float DELA_TAP2 = MS_TO_SAMPSF(8.9f);
constexpr const float DELD_TAP2 = MS_TO_SAMPSF(11.8f);

constexpr const float DELA_TAP3 = MS_TO_SAMPSF(99.7f);
constexpr const float DELD_TAP3 = MS_TO_SAMPSF(121.7f);

constexpr const float DELA_TAP4 = MS_TO_SAMPSF(70.8f);
constexpr const float DELD_TAP4 = MS_TO_SAMPSF(66.8f);

constexpr const float DELC_TAP2 = MS_TO_SAMPSF(67.f);
constexpr const float DELF_TAP2 = MS_TO_SAMPSF(89.7f);

constexpr const float DELC_TAP3 = MS_TO_SAMPSF(4.1f);
constexpr const float DELF_TAP3 = MS_TO_SAMPSF(35.8f);

constexpr const uint32_t APL_TAP1_TIME = MS_TO_SAMPSF(89.24f);
constexpr const uint32_t APL_TAP2_TIME = MS_TO_SAMPSF(64.2f);
constexpr const uint32_t APL_TAP3_TIME = MS_TO_SAMPSF(11.2f);

constexpr const uint32_t APR_TAP1_TIME = MS_TO_SAMPSF(60.48f);
constexpr const uint32_t APR_TAP2_TIME = MS_TO_SAMPSF(6.3f);
constexpr const uint32_t APR_TAP3_TIME = MS_TO_SAMPSF(41.2f);

} // namespace demo

#endif /* Defs.h */
