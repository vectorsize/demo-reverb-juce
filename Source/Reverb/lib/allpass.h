//
//  allpass.h
//
//  Created by Victor Saiz on 29/03/2021.
//

#ifndef allpass_h
#define allpass_h

#include "lfo.h"
#include <stdint.h>

template <typename T, uint32_t max_size> class Allpass {
public:
  // non modulated constructor
  Allpass(T *buffer, uint32_t delIndex, float gain)
      : mGain(gain), mLfoFreq(0), mWritePos(0), mDelIndex(delIndex),
        mBuffer(buffer), mLfoMult(0) {}

  // modulated constructor
  Allpass(T *buffer, uint32_t delIndex, float gain, float lfoFreq)
      : mGain(gain), mLfoFreq(lfoFreq), mWritePos(0), mDelIndex(delIndex),
        mBuffer(buffer) {}

  ~Allpass() = default;

  uint32_t getIdx(const float delIndex) {
    // if no modulation return plain delay else modulate with LFO
    auto idx = mLfoMult > 0 ? (mWritePos - delIndex) + mLfo.process() * mLfoMult
                            : mWritePos - delIndex;

    if (idx < 0)
      idx += max_size;
    if (idx >= max_size)
      idx -= max_size;

    return static_cast<uint32_t>(idx);
  }

  inline T process(float input) {
    uint32_t idx = getIdx(mDelIndex);
    T delayRead = mBuffer[idx];
    T feed = input + delayRead * mGain;
    T out = delayRead + (feed * (mGain * -1));

    mBuffer[mWritePos] = feed;

    mWritePos++;
    if (mWritePos >= max_size)
      mWritePos = 0;

    return out;
  }

  inline T tap(const float index) { return mBuffer[getIdx(index)]; }

private:
  float mGain = 0.0f;
  float mLfoFreq = 0.0f;
  uint32_t mWritePos;
  float mDelIndex;
  T *mBuffer;
  LFO mLfo;
  uint8_t mLfoMult = 4;
};

#endif /* allpass_h */
