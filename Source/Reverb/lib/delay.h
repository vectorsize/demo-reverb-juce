#pragma once
#ifndef Delay_H
#define Delay_H
#include "defines.h"
#include <stdlib.h>

// BASED ON DAISYDELAYLINE
template <typename T, size_t maxSize> class DelayLine {
public:
  DelayLine() = default;
  ~DelayLine() = default;

  void Init() { Reset(); }

  void Reset() {
    // clear buffers
    for (size_t i = 0; i < maxSize; i++) {
      mLine[i] = static_cast<T>(0);
    }
    // init
    mWritePtr = 0;
    mDelay = 1;
  }

  // sets delay time in samples / mfrac reset
  inline void SetDelay(const uint32_t delay) {
    mFrac = 0.0f;
    mDelay = delay < maxSize ? delay : maxSize - 1;
  }

  // sets delay time in samples / mfrac is precalculated
  inline void SetDelay(const float delay) {
    int32_t iDelay = static_cast<int32_t>(delay);
    mFrac = delay - static_cast<float>(iDelay);
    mDelay = static_cast<size_t>(iDelay) < maxSize ? iDelay : maxSize - 1;
  }

  // write value and inc position
  inline void Write(const T sample) {
    mLine[mWritePtr] = sample;
    mWritePtr = (mWritePtr - 1 + maxSize) % maxSize;
  }

  // returns next sample in buffer
  inline const T Read() const {
    T a = mLine[(mWritePtr + mDelay) % maxSize];
    T b = mLine[(mWritePtr + mDelay + 1) % maxSize];
    return a + (b - a) * mFrac;
  }

  // returns specific sample in buffer interpolated
  inline const T Read(const uint32_t delay) {
    SetDelay(delay);
    T a = mLine[(mWritePtr + mDelay) % maxSize];
    T b = mLine[(mWritePtr + mDelay + 1) % maxSize];
    return a + (b - a) * mFrac;
  }

private:
  float mFrac;
  size_t mWritePtr;
  size_t mDelay;
  T mLine[maxSize];
};

#endif /* Delay_h */
