//
//  Reverb.cpp
//  DattorroHarware
//
//  Created by Victor Saiz on 26/11/2020.
//

#include "Reverb.hpp"

void demo::Reverb::setup() {
  kPreDelay *= static_cast<float>(PREDELAY_BUFFER_SIZE);
  kPreDelaySmooth = kPreDelay;
  mPredelayL.Init();
  mPredelayR.Init();
  mDela.Init();
  mDelc.Init();
  mDeld.Init();
  mDelf.Init();
  mDela.SetDelay(DELA_TAP1);
  mDelc.SetDelay(DELC_TAP1);
  mDeld.SetDelay(DELD_TAP1);
  mDelf.SetDelay(DELF_TAP1);
}

void demo::Reverb::setPredelay(const float ctl) {
  kPreDelay = map<float>(ctl, 0.f, 1.f, 0.f, PREDELAY_BUFFER_SIZE);
}

void demo::Reverb::setBandwidth(const float ctl) { kBandwidth = ctl; }
void demo::Reverb::setDamping(const float ctl) { kDamping = ctl; }
void demo::Reverb::setTime(const float ctl) { kTime = sqrt(ctl); }
void demo::Reverb::setMix(const float ctl) { kDrywet = ctl; }

void demo::Reverb::processSamples(const float sL, const float sR) {
  float outL = 0.0f;
  float outR = 0.0f;

  //  Patch
  // ---

  // Predelay
  mPredelayL.Write(sL);
  mPredelayR.Write(sR);

  // smooth delay time
  kPreDelaySmooth = kPreDelaySmooth - 0.001f * (kPreDelaySmooth - kPreDelay);
  mPredelayL.SetDelay(kPreDelaySmooth + mDelayFloor);
  mPredelayR.SetDelay(kPreDelaySmooth + mDelayFloor);

  const float predelayLOutput = mPredelayL.Read();
  const float predelayROutput = mPredelayR.Read();

  // Bandwidth filtering
  mFilterInL.setFc(inv<float>(kBandwidth));
  mFilterInR.setFc(inv<float>(kBandwidth));

  const float filterInputLOut =
      mFilterInL.process(predelayLOutput * kBandwidth);
  const float filterInputROut =
      mFilterInR.process(predelayROutput * kBandwidth);

  // Diffusion L
  const float allPass1LOutput = mAllPass1L.process(filterInputLOut);
  const float allPass2LOutput = mAllPass2L.process(allPass1LOutput);
  const float allPass3LOutput = mAllPass3L.process(allPass2LOutput);
  const float allPass4LOutput = mAllPass4L.process(allPass3LOutput);
  // Diffusion R
  const float allPass1ROutput = mAllPass1R.process(filterInputROut);
  const float allPass2ROutput = mAllPass2R.process(allPass1ROutput);
  const float allPass3ROutput = mAllPass3R.process(allPass2ROutput);
  const float allPass4ROutput = mAllPass4R.process(allPass3ROutput);

  // Tank
  const float modAllpassLOutput =
      mModAllpassL.process((mDelf.Read(DELF_TAP1) * kTime) + allPass4LOutput);
  const float modAllpassROutput =
      mModAllpassR.process((mDelc.Read(DELC_TAP1) * kTime) + allPass4ROutput);

  mDela.Write(modAllpassLOutput);
  mDeld.Write(modAllpassROutput);

  mFilterOutL.setFc(kDamping);
  const float allpass2TapLOut = mAllpass2TapL.process(
      mFilterOutL.process(mDela.Read(DELA_TAP1) * inv<float>(kDamping)));
  mDelc.Write(allpass2TapLOut * kTime);

  mFilterOutR.setFc(kDamping);
  const float allpass2TapROut = mAllpass2TapR.process(
      mFilterOutR.process(mDeld.Read(DELD_TAP1) * inv<float>(kDamping)));
  mDelf.Write(allpass2TapROut * kTime);

  // Mix
  // ---
  outL = mDela.Read(DELA_TAP2);
  outL += mDela.Read(DELA_TAP3);
  outL -= mAllpass2TapL.tap(APL_TAP2_TIME);
  outL += mDelc.Read(DELC_TAP2);
  outL += mDeld.Read(DELD_TAP4);
  outL -= mAllpass2TapL.tap(APL_TAP2_TIME);
  outL += mDelf.Read(DELF_TAP3);

  outR = mDeld.Read(DELD_TAP2);
  outR += mDeld.Read(DELD_TAP3);
  outR -= mAllpass2TapR.tap(APR_TAP3_TIME);
  outR += mDelf.Read(DELF_TAP2);
  outR += mDela.Read(DELA_TAP4);
  outR -= mAllpass2TapR.tap(APR_TAP3_TIME);
  outR += mDelc.Read(DELC_TAP3);

  // Global Attenuation
  // ---
  outL *= KGainAttenuator;
  outR *= KGainAttenuator;

  // DryWet
  // ---
  outputL = (sL * inv<float>(kDrywet)) + (outL * kDrywet);
  outputR = (sR * inv<float>(kDrywet)) + (outR * kDrywet);
}
